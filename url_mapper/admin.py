from django.contrib import admin

from url_mapper.models import URL

admin.site.register(URL)
