from django.urls import path

from url_mapper import views

urlpatterns = [
    # Creates new URL on DB if not exist or returns one if already exist
    path("create_short/", views.get_or_create_short_url, name="get-or-create-shorturl"),
    # Decodes short provided URL returning Long one if exist in DB, otherwise raises 404 Not Found
    path("decode_short/", views.get_long_url, name="get-longurl"),
]
