from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from url_mapper.models import URL
from url_mapper.url_operations import generate_short_url


# As I understood from the instructions, I should keep things as simple as possible,
# so I decided to use function based view and not delegating logic associated with
# url_mapper to additional package


@api_view(["POST"])
def get_or_create_short_url(request: Request) -> Response:
    """
    Creates short URL mapped on the URL provided in the request 'url' parameter.
    Returns short URL if long URL has been already registered.
    """

    # I assume that url parameter is provided correctly
    url_provided = request.data.get("url")

    # I would probably delegate logic above to 'services' related to url_mapper additionally
    obj, created = URL.objects.get_or_create(
        long_url=url_provided,
        defaults={"short_url": generate_short_url()},
    )

    return Response(
        data={"url": obj.short_url},
        status=status.HTTP_200_OK,
    )


@api_view(["POST"])
def get_long_url(request: Request) -> Response:
    """
    Returns the long URL associated with short URL provided.
    If short URL is not registered (there is no long URL mapped) the 404 is returned in the response
    """

    # I assume that url parameter is provided correctly
    url_provided = request.data.get("url")

    # I would probably delegate logic above to 'services' related to url_mapper additionally
    obj = get_object_or_404(URL, short_url=url_provided)

    return Response(data={"url": obj.long_url}, status=status.HTTP_200_OK)
