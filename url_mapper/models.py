from django.db import models


class URL(models.Model):
    # I assume that 512 chars limit is enough according to URL example provided in the instructions
    long_url = models.URLField(max_length=512)
    short_url = models.URLField(max_length=512)
