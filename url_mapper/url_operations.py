import random
import string

from django.conf import settings

from url_mapper.models import URL


def generate_short_url() -> str:
    """
    Generates and returns unique short URL.
    """
    while 1:
        # I assume that 5 characters of the short URL is desired according to the example from instructions
        generated_short_url = f"http://{settings.HOSTNAME}/{''.join(random.choices(string.ascii_letters, k=5))}"

        try:
            URL.objects.get(short_url=generated_short_url)

        except URL.DoesNotExist:
            return generated_short_url
