URL mapper

Simple backend application that allows to create short URL from
the original one provided. Additionally providing the short one
that was generated, returns the original one again.

# Author
Bartosz Ludniewski

# Endpoints:
- POST /api/v1/mapper/create_short/
- POST /api/v1/mapper/decode_short/

# Parameters
- url (as string) - long url to be converted into short one
in case of '/api/v1/mapper/create_short/' or the short one
to be mapped into long one in case of '/api/v1/mapper/decode_short/'

# Authentication
Skipped according to instructions

# Setup
- Create virtual environment
- Install recursive packages from requirements.txt file
- Migrate DB structure
- Run server