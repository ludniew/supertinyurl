import pytest
from faker import Faker

from django.urls import reverse

from url_mapper.models import URL


fake = Faker()


@pytest.mark.django_db
def test_POST_get_or_create_short_url_should_create_and_return_short_url_whether_not_already_exist(
    api_client,
):
    target_url = reverse("get-or-create-shorturl")
    payload = {"url": f"{fake.url()}{fake.uri_path()}"}
    assert len(URL.objects.all()) == 0

    response = api_client.post(path=target_url, data=payload)
    urls_db = URL.objects.all()

    assert response.status_code == 200
    assert response.data == {"url": urls_db[0].short_url}
    assert len(urls_db) == 1
    assert urls_db[0].long_url == payload["url"]
    assert urls_db[0].short_url != payload["url"]
    assert urls_db[0].short_url != ""


@pytest.mark.django_db
def test_POST_get_or_create_short_url_should_return_short_url_whether_already_exist(
    api_client,
):
    url = URL.objects.create(
        long_url="http://localhost/some/long/url",
        short_url="http://localhost/short",
    )
    target_url = reverse("get-or-create-shorturl")
    payload = {"url": url.long_url}
    assert len(URL.objects.all()) == 1

    response = api_client.post(path=target_url, data=payload)
    urls_db = URL.objects.all()

    assert response.status_code == 200
    assert response.data == {"url": url.short_url}
    assert len(urls_db) == 1
    assert urls_db[0] == url


@pytest.mark.django_db
def test_POST_get_long_url_should_return_long_url_whether_exist(api_client):
    url = URL.objects.create(
        long_url="http://localhost/some/long/url",
        short_url="http://localhost/short",
    )
    target_url = reverse("get-longurl")
    payload = {"url": url.short_url}
    assert len(URL.objects.all()) == 1

    response = api_client.post(path=target_url, data=payload)
    urls_db = URL.objects.all()

    assert response.status_code == 200
    assert response.data == {"url": url.long_url}
    assert len(urls_db) == 1
    assert urls_db[0] == url


@pytest.mark.django_db
def test_POST_get_long_url_should_return_404_whether_url_with_short_url_does_not_exist(
    api_client,
):
    target_url = reverse("get-longurl")
    payload = {"url": "http://example.com/not/existing/url"}
    assert len(URL.objects.all()) == 0

    response = api_client.post(path=target_url, data=payload)
    urls_db = URL.objects.all()

    assert response.status_code == 404
    assert len(urls_db) == 0
