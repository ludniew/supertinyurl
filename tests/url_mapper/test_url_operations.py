import pytest

from url_mapper.url_operations import generate_short_url


@pytest.mark.django_db
def test_generate_short_url_when_host_provided_then_short_url_returned():
    url_generated = generate_short_url()

    assert len(url_generated.split("/")[-1]) == 5
